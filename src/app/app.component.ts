import { Component, OnInit } from '@angular/core';
import { BotStatusService, BotStatus } from './services/status.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  title = 'app';

  constructor(private botStatusService: BotStatusService)
  {}

  ngOnInit(): void {
    this.botStatusService.getAll().subscribe((statuses:BotStatus[]) => {
      alert(JSON.stringify(statuses));
    })
  }

}
