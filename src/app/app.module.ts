import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';

import { HttpModule } from '@angular/http';

import { BotStatusService } from './services/status.service';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpModule
    
  ],
  providers: [BotStatusService],
  bootstrap: [AppComponent]
})
export class AppModule { }
