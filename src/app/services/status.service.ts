import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { environment } from '../../environments/environment';

@Injectable()
export class BotStatusService {
    private http: Http;            

    constructor(http: Http) {
        this.http = http;
    }

    getAll(): Observable<BotStatus[]> {
        var model: BotStatus[];

        return this.http.get(environment.api + '/status')
            .map((res) => { return res.json(); })
    }
}

export class BotStatus {
    name: string;
    status: string;
}
